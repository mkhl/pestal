---
---
<!-- 50 -->

> Then I bestow this advice. Extend a smile and the hand of friendship to your enemies. When they clasp your hand, strike them with the dagger in your other hand, else they will surely do the same to you.

<!-- Journal entry:
The book advised me: 'Extend a smile and the hand of friendship to your enemies. When they clasp your hand, strike them with the dagger in your other hand, else they will surely do the same to you.' -->

* [Give me some more advice.](/15/blood/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](index.html)
