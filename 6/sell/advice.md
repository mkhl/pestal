---
---
<!-- 42 -->

You feel a faint sneer curl up the corner of your mouth. You quickly repress it.

> Then I bestow this advice: Separate yourself from small-minded fools and the weak by recognizing that you have a destiny. This alone gives you authority to tread where others would fear to go.

<!-- Journal entry:
The book advised me: 'Separate yourself from small-minded fools and the weak by recognizing that you have a destiny. This alone gives you authority to tread where others would fear to go.' -->

* [Give me some more advice.](/7/sell/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](index.html)
