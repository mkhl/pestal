---
---
<!-- 49 -->

> Then I bestow this advice. Nurture fear in others, for only fear rules men. Nothing else leaves deeper scars.

<!-- Journal entry:
The book advised me: 'Nurture fear in others, for only fear rules men. Nothing else leaves deeper scars.' -->

* [Give me some more advice.](/14/blood/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](index.html)
