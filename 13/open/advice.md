---
---
<!-- 19 -->

> Then I bestow this advice, my master. Nurture fear in others, for only fear rules men. Nothing else leaves deeper scars.

<!-- Journal entry:
The book advised me: 'Nurture fear in others, for only fear rules men. Nothing else leaves deeper scars.' -->

* [Give me some more advice.](/14/open/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](enough.html)
