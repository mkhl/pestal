---
---
<!-- 67 -->

> Excellent. You have proven yourself a worthy student. This, then, is the final spell I can teach you.

It mutters some strange syllables, and you hear a strange howl in your heart. You know the spell _Power Word: Kill_.

* [Thanks, book. What now?](/done/talk.html)
* [Farewell, book.](/done/index.html)
