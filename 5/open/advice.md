---
---
<!-- 11 -->

> Then I bestow this advice, my master. Beware the common view of love. Love must often be cruel in order so that it may rise above the merely sentimental.

<!-- Journal entry:
The book advised me: 'Beware the common view of love. Love must often be cruel in order so that it may rise above the merely sentimental.' -->

* [Give me some more advice.](/6/open/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](enough.html)
