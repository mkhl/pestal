---
---
<!-- 21 -->

As the book speaks these words, you feel a chill weight settle on your heart. You feel somehow tainted:

> These are my last words of advice, my master. Weigh them carefully. Remember that you can expunge anything you find undesirable. You need only have the will.

<!-- Journal entry:
The book advised me: 'Remember that you can expunge anything you find undesirable. You need only have the will.' -->

* [Give me some more advice.](/16/open/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](enough.html)
