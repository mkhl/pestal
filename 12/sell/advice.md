---
---
<!-- 48 -->

> Then I bestow this advice. Know the meaning of mercy, use it to your advantage&hellip; but do not practice it.

<!-- Journal entry:
The book advised me: 'Know the meaning of mercy, use it to your advantage&hellip;but do not practice it.' -->

* [Give me some more advice.](/13/sell/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](index.html)
