---
---
# Mantuok's book

This black-bound tome seems to radiate a slight rotting smell, and the covers are unpleasant to the touch - like uncured human flesh left out in the sun. Rusted buckles on the cover seal it against the elements.

<form action="/0/open/open.html" method="get" accept-charset="utf-8">
    <input type="submit" value="Open the book.">
</form>
