---
---
<!-- 44 -->

> Then I bestow this advice. To become powerful, you must be willing to sacrifice: whether it is parts of yourself or things precious to you. Power is a bargain: One must give in order to receive.

<!-- Journal entry:
The book advised me: 'To become powerful, you must be willing to sacrifice: whether it is parts of yourself or things precious to you. Power is a bargain: One must give in order to receive.' -->

* [Give me some more advice.](/9/sell/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](index.html)
