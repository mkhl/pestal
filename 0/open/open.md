---
---
<!-- 0 -->

As you touch this book, you hear a voice:

> I greet thee, master. How may this humble tome serve you?

* [You can talk?](you-can-talk.html)
* [You can't. Stop talking to me.](enough.html)
