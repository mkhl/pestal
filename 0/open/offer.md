---
---
<!-- 3 -->

> I can grant you abilities that will allow you greater latitude in life's vicissitudes, master, and I can offer advice to you.

* [What abilities do you mean?](powers.html)
* [Give me some advice.](/1/open/advice.html)
* [Forget it. Answer some other questions for me.](questions.html)
