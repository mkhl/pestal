---
---
<!-- 2 -->

> That I can, master, in many different languages. Only you should take care that none see us conversing, for they cannot hear me. And, master, speech is but one of my powers.

* [What can you do? What are your powers?](offer.html)
* [How did Mantuok come to possess you?](mantuok.html)
* [Tell me about yourself.](yourself.html)
* [That's enough for now.](enough.html)
