---
---
<!-- 55 -->

> In time, in time! You must have patience! You have already shown yourself willing to do what it takes to achieve this power, and you must realize that you WILL pay for this power.

* [What do I have to do?](../final/sacrifice.html)
* [I've paid enough. Forget it.](enough.html)
