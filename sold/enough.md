---
---
<!-- 56 -->

The book speaks scornfully.

> To have come so far, only to back out now. Shameful. You are not as strong as I had thought.

* [Be that as it may. Answer some questions for me.](questions.html)
* [Fine. What do I have to do?](../final/sacrifice.html)
* [Be that as it may. Farewell.](../final/index.html)
