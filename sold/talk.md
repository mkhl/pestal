---
---
<!-- 34 -->

> Master, have you completed the second sacrifice? There is an air about you that says you have. You have done well; you are a swift learner. Listen to these words and learn still more.

The Book speaks some unintelligible syllables and you feel a burning sensation within your chest. You now know the spell _Adder's Kiss_.

<!-- Journal entry:
The book has given me the spell Adder's Kiss, in exchange for selling one of my companions into slavery. -->

* [Do you have more to teach me, Book?](offer.html)
* [My thanks. Farewell.](../final/index.html)
