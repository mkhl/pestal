---
---
<!-- 8 -->

> Then I bestow this advice, my master. If they claim friendship, then let them prove themselves. Words carry little weight compared to action. If friends they be, then they should be willing to die for you.

<!-- Journal entry:
The book advised me: 'If they claim friendship, then let them prove themselves. Words carry little weight compared to action. If friends they be, then they should be willing to die for you.' -->

* [Give me some more advice.](/3/open/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](enough.html)
