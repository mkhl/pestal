---
---
<!-- 13 -->

> Then I bestow this advice, my master. There are two secrets to becoming great. One is never to reveal all that you know.

<!-- Journal entry:
The book advised me: 'There are two secrets to becoming great. One is never to reveal all that you know.' -->

* [Give me some more advice.](/8/open/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](enough.html)
