---
---
<!-- 4 -->

> What do you wish to know, master?

* [What can you do? What are your powers?](offer.html)
* [How did Mantuok come to possess you?](mantuok.html)
* [Tell me about yourself.](yourself.html)
* [Give me some advice.](/8/open/advice.html)
* [That's enough for now.](enough.html)
