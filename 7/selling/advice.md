---
---
<!-- 43 -->

> Then I bestow this advice. There are two secrets to becoming great. One is never to reveal all that you know.

<!-- Journal entry:
The book advised me: 'There are two secrets to becoming great. One is never to reveal all that you know.' -->

* [Give me some more advice.](/8/selling/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](index.html)
