---
---
<!-- 36 -->

> You wish my knowledge&hellip; master?

* [Yes](advice.html)
* [Answer some questions for me instead.](questions.html)
* [That's enough for now.](index.html)
