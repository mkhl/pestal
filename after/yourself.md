---
---
<!-- 54 -->

> I am power, master, and I can teach you more.

* [How?](offer.html)
* [Refresh my memory.](sacrifice.html)
* [Not interested right now. Answer some other questions for me, instead.](questions.html)
* [Not interested. Goodbye, book.](index.html)
