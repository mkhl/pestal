---
---
<!-- 33 -->

> Master. Have you decided that perhaps you wish to continue your education? The offer of greater insight and ability remains open to you if you will but complete the second sacrifice.

* [All right, I'll do it.](choose.html)
* [I'll think about it. Give me some advice.](knowledge.html)
* [I had some questions I needed answered first.](questions.html)
* [Not right now. Farewell.](index.html)
