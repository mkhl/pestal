---
---
<!-- 45 -->

Your blood seems to move more sluggishly within your veins as the book speaks.

> Then I bestow this advice. Honor and virtue are concepts that kill men and kill the spirit. They are only abstracts that drive men to strangeness and death. Be true to yourself and obey only your own will.

<!-- Journal entry:
The book advised me: 'Honor and virtue are concepts that kill men and kill the spirit. They are only abstracts that drive men to strangeness and death. Be true to yourself and obey only your own will.' -->

* [Give me some more advice.](/10/blood/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](index.html)
