---
---
<!-- 31 -->

> I am saddened that you do not think my Powers worth such a small price. Do not let doubt and confusion shackle you, master&hellip; but I speak out of turn. Remember that the offer remains open to you.

* [I'll do it. Give me some advice, will you?](knowledge.html)
* [I'll think about it. I need some more answers from you.](questions.html)
* [I'll think about it. Farewell.](index.html)
