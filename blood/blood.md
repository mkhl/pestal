---
---
<!-- 25 -->

You cut yourself and allow a few drops of blood to drip upon the page. A strange chill settles in your bones, and you feel empty and hurt. At the same time, there is a feeling of triumph: You have learned a new spell: _Blindness_.

> This is your Power now&hellip; master. Know that this is but the first Power that lies in my pages.

<!-- Journal entry:
This strange book I have picked up has asked me to spill some of my blood onto its pages. I have done so, and gained a spell. It has promised me more power, as well. -->

* [Tell me of these other powers.](offer.html)
* [Answer some more questions for me.](questions.html)
* [I want some advice from you.](knowledge.html)
* [I need nothing more right now.](enough.html)
