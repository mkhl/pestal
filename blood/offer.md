---
---
<!-- 29 -->

The voice of the Book seems saddened.

> Unfortunately, master, the laws that bind me demand a greater sacrifice in return for greater power. I would grant these powers to you freely, but as you can see, I am bound.

* [What is the sacrifice?](../sell/sacrifice.html)
* [Forget it, then.](nope.html)
