---
---
<!-- 53 -->

The book sighs:

> The diseased rat-mage unearthed me in the catacombs. He pried me from the grip of my former master. He brought me to his nest, but his power was limited, and he understood little of my teachings.

* [Tell me of your powers.](offer.html)
* [I see. Answer some other questions for me.](questions.html)
* [True. Farewell.](index.html)
