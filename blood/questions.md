---
---
<!-- 35 -->

> You require answers?

* [How did Mantuok come to possess you?](mantuok.html)
* [Tell me about yourself.](yourself.html)
* [Can you give me advice?](knowledge.html)
* [Do you have more powers for me?](offer.html)
* [No more. Farewell.](index.html)
