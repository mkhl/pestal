---
---
<!-- 32 -->

> Master. Have you decided that perhaps you wish to continue your education? The offer of greater insight and ability remains open to you.

* [Tell me what I have to do.](../sell/sacrifice.html)
* [Not yet. Answer some other questions for me.](questions.html)
* [Not yet. I want some advice from you.](knowledge.html)
* [No. Farewell.](index.html)
