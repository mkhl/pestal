---
---
<!-- 66 -->

## Journal entry:

I have just agreed to slay one of my companions in exchange for power from the book.

<form action="../killed/index.html" method="get" accept-charset="utf-8">
    <input type="submit" value="Murder one of your companions.">
</form>
<form action="../kill/index.html" method="get" accept-charset="utf-8">
    <input type="submit" value="Spare your friends.">
</form>
