---
---
<!-- 60 -->

> I can grant you one more power. You have shown yourself willing to do what it takes to finish a job. Your final task is to slay one of your companions, by yourself, without spells.

* [How about&hellip;?](how-about.html)
* [I don't think so.](none.html)
* [I have no companions.](alone.html)
