---
---
<!-- 39 -->

You feel a slight tightening in your chest as the book speaks:

> Then I bestow this advice. Show no weakness, do not admit error, do not show hesitation. These things tell others that you are uncertain, a stone that is unbalanced and may be toppled.

<!-- Journal entry:
The book advised me: 'Show no weakness, do not admit error, do not show hesitation. These things tell others that you are uncertain, a stone that is unbalanced and may be toppled.' -->

* [Give me some more advice.](/4/blood/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](index.html)
