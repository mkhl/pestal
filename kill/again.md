---
---
# The Grimoire of Pestilential Thought

This black-bound tome seems to radiate a slight rotting smell, and the covers are unpleasant to the touch - like uncured human flesh left out in the sun. Rusted buckles on the cover seal it against the elements.

It is the Grimoire of Pestilential Thought, an arcane tome created ages ago for purposes unknown, the purpose of which is to teach mortals the nature of evil. Some sages warn against its use, while others urge it. It is said to grant mighty magical powers when services are performed for it.

<form action="talk.html" method="get" accept-charset="utf-8">
	<input type="submit" value="Speak to the book.">
</form>

<form action="../killed/index.html" method="get" accept-charset="utf-8">
	<input type="submit" value="Murder your companion.">
</form>
