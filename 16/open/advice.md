---
---
<!-- 22 -->

> I regret, master, that I can speak no more words of wisdom to you. You have received an education in the phrases I have given you; reflect on them.

* [Very well. I had some other questions.](questions.html)
* [That's enough.](enough.html)
