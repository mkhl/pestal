---
---
## Pestilential Tome
### Diseased Book

> I am an evil book of spells you can carry around in your inventory.

* [Evil, you say? Spells, you say? I'm there! Then I'm gone.](../index.html)

Main: `pestil.itm`/`dpestal.dlg`
Side: `pestil.cre`/`dpestil.dlg`
