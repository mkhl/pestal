---
---
<!-- 37 -->

> I bestow this advice: Be wary of those you keep close to you. Never take them fully into your confidence, and never dilute your strength by sharing it. Many emperors I have known have done such, and I do not wish you to suffer the same fate.

<!-- Journal entry:
The book advised me: 'Be wary of those you keep close to you. Never take them fully into your confidence, and never dilute your strength by sharing it. Many emperors I have known have done such, and I do not wish you to suffer the same fate.' -->

* [Give me some more advice.](/2/sell/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](index.html)
