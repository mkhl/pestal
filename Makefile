.DEFAULT: all

JEKYLL_VERSION = 3.8
JEKYLL = docker run --rm -it --volume="$(PWD):/srv/jekyll:Z" --publish=4000:4000 jekyll/jekyll:$(JEKYLL_VERSION) jekyll
sources := index.md $(wildcard [0-9]*/*/*.md)
# objects := $(patsubst %.md,%.html,$(sources))
objects := _site
assets := _config.yml $(wildcard _layouts/*) $(wildcard css/*)

.PHONY: all
all: $(objects)

_site: $(sources) $(assets)
	$(JEKYLL) build

.PHONY: clean
clean:
	$(RM) -r $(objects)

.PHONY: serve
serve:
	$(JEKYLL) serve
