---
---
<!-- 40 -->

> Then I bestow this advice. Beware mercy. A turned back is no defense against an assassin's knife.

<!-- Journal entry:
The book advised me: 'Beware mercy. A turned back is no defense against an assassin's knife.' -->

* [Give me some more advice.](/5/selling/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](index.html)
