---
---
<!-- 16 -->

> Then I bestow this advice, my master. Do not suffer one who has hurt you to live. To do so is an open admission of weakness.

<!-- Journal entry:
The book advised me: 'Do not suffer one who has hurt you to live. To do so is an open admission of weakness.' -->

* [Give me some more advice.](/11/open/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](enough.html)
