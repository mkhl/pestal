---
---
<!-- 24 -->

> The service required is a trifling one, my master. I would barely mention it if it were not for the sake of the laws that bind me to these pages&hellip;

The book pauses.

> To unlock my Power, you must sacrifice but a small bit of yourself, my master. You must spill blood upon the first of my pages. This act will serve to strengthen the chain between us and allow me to help you reach your full potential. It is a small price to pay for the spells that will come to be at your disposal.

* <form action="../blood/blood.html" method="get" accept-charset="utf-8">
    <input type="submit" value="Spill your blood.">
  </form>
* [No.](nope.html)
