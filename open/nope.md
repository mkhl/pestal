---
---
<!-- 27 -->

> It is but a small sacrifice, master, for a great gain&hellip; but it is your choice. The offer remains open, master.

<!-- Journal entry:
This strange book I have found has asked me to give up a small piece of myself in exchange for power. I declined. The option is still open, however. -->

* [Good. Answer some other questions for me.](questions.html)
* [Good. Farewell.](enough.html)
