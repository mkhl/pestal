---
---
<!-- 23 -->

> These are Powers, my master, that can improve your life immeasurably. Unfortunately, the laws that bind me&hellip;they demand a small service in recompense for these Powers.

* [A small service? Like what?](sacrifice.html)
* [Then I wish nothing.](nope.html)
