---
---
<!-- 6 -->

> The question you ask is not a simple one, my master, but your servant shall seek to answer it as best it can. I am a codex of moments-now-forgotten, ideas-now-lost, and thoughts of other times and other worlds. Within my pages lies the lore of forgotten civilizations. In essence, my master, and this is the only thing of import: within my pages lie POWER. This Power can be yours&hellip; my master.

* [Tell me of your powers.](powers.html)
* [Not interested right now. Answer some other questions for me, instead.](questions.html)
* [Not interested. Goodbye, book.](index.html)
