---
---
<!-- 5 -->

> The diseased rat-mage unearthed me in the catacombs, my master. He pried me from the grip of my former master, now long since dead. He brought me to his nest so that he might learn from me. His power was limited, however&hellip; it was difficult to speak to him, and he understood little of my teachings.

* [Tell me of your powers.](offer.html)
* [I see. Answer some other questions for me.](questions.html)
* [True. Farewell.](index.html)
