---
---
<!-- 17 -->

> Then I bestow this advice, my master. The soul of man is a sea of emotions. Often man struggles to bring order to these emotions, but it is a false order. Man's natural state is chaos, and it is a state that should be surrendered to.

<!-- Journal entry:
The book advised me: 'The soul of man is a sea of emotions. Often man struggles to bring order to these emotions, but it is a false order. Man's natural state is chaos, and it is a state that should be surrendered to.' -->

* [Give me some more advice.](/12/open/advice.html)
* [Very well. I had some other questions.](questions.html)
* [That's enough.](enough.html)
