---
---
<!-- 33 -->

> Master. Have you decided that perhaps you wish to continue your education? The offer of greater insight and ability remains open to you if you will but complete the second sacrifice.

* [Sure, I'll do it.](choose.html)
* [All right, I'll do it. Now answer some questions for me.](questions.html)
* [Sounds fair. I'll do it. I need some more advice from you.](knowledge.html)
* [Not right now. Farewell.](index.html)
