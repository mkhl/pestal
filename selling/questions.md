---
---
<!-- 35 -->

> You require answers?

* [How did Mantuok come to possess you?](mantuok.html)
* [Tell me about yourself.](yourself.html)
* [Can you give me advice?](knowledge.html)
* [What was the sacrifice I had to make for the new power?](sacrifice.html)
* [No more. Farewell.](index.html)
