---
---
<!-- 30 -->

> The second sacrifice is that you sell one of your companions into durance, bondage, slavery. This will signify the binding between you and the power – your companion represents the Power placed at your disposal. There are those in the Clerk's Ward, I am given to understand, who will aid you in this quest. Seek out Vrischika the importer.

<!-- Journal entry:
The book has offered me another spell in exchange for selling one of my companions into slavery. I am to seek out *Virischika the Importer*, in the Clerk's Ward, if I wish to see this power. -->

<!-- Journal entry:
The book has offered me another spell in exchange for selling one of my companions into slavery. I am to seek out *Virischika the importer*, if I wish to see this power. I have agreed to do it. -->

* [I'll do it.](choose.html)
* [I'll do it. Give me some advice, will you?](knowledge.html)
* [I'll do it. But I need some more answers from you first.](questions.html)
* [I'll think about it. Farewell.](index.html)
